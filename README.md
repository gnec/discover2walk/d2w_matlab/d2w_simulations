# Discover2Walk Matlab Simulations

In this repository you will find MATLAB-Simulink based simulations of the Discover2Walk (D2W) robotic sistem. To run the simulations you will need to download Simulink, Simscape and SimscapeMultibody.

The main modules of the D2W are:
1. **lower_system**: Cable-driven robotic module in charge of controlling the ankles/feet positions and orientations during gait or foot rehabilitation.
2. **pelvis_system**:  Cable-driven robotic module in charge of controlling the pelvis orientation, position and users body weight support.

## MATLAB Packages required
1. [Simulink](https://es.mathworks.com/products/simulink.html)
2. [Simscape](https://es.mathworks.com/products/simscape.html)
3. [SimscapeMultibody](https://es.mathworks.com/products/simscape-multibody.html)
4. [Control System Toolbox](https://es.mathworks.com/products/control.html)


## Open repository in MATLAB-Simulink
To open this repository in MATLAB-Simulink and keep track of the changes you need to create a matlab project folliwing these instructions:

1. Open MATLAB, in "home" select "New" and "Simulink Model". This action will open a menu with several options.
2. Select "Project from Git".
3. In the "Repository path" select the https direction of this repository "https://gitlab.com/gnec/discover2walk/d2w_matlab/d2w_simulations.git" and clic in "Retrieve". It will ask you to create a specified sandbox folder, create it. 
4. Select the name of the local project: "d2w_simulations" and continue with the set up, you do not need to create or add any more files, just click next and finish. 
5. At this point you have access to the simulink models and any git add/comit/fetch/push/pull... command to keep track of your changes. 


## Branches
There are 3 subbranches in case you want to work with the latest:
- *pelvis*: 4 cables solution to control the pelvis.
- *ankles*: 4 cables solution to control the ankles positions x,y,z control.
- *lower_4_cables*: 4 cables solution for to controll the x,y and pitch of the feet.
- *lower_8_cables*: 8 cables solution for the 6 DOF control of the feet.
